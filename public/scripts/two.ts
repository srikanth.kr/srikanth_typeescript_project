export{}
const fs=require('fs');
const csv=require('csv-parser');
const RegistrationPerYear :any= {}
fs.createReadStream('/home/srikanthkr/typescript_project/source_data/company_master_data_upto_Mar_2015_Maharashtra.csv')
.pipe(csv())
.on('data',(res:any)=>{
    const date = res.DATE_OF_REGISTRATION.split('-');
    if (date[2]>= 2001 && date[2] <= 2018) {
      if (!RegistrationPerYear[date[2]]) {
        RegistrationPerYear[date[2]] = 1;
      } else {
        RegistrationPerYear[date[2]] += 1;
      }
    }      
})
.on('end',()=>{
    console.log(RegistrationPerYear);
    fs.writeFile('/home/srikanthkr/typescript_project/frontend/json/Number_OfRegistrationPerYear.json',JSON.stringify(RegistrationPerYear),() => {});
});