"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var fs = require('fs');
var csv = require('csv-parser');
var PBARegistrations2015 = {};
var PBARegistrationsPerYear = {};
fs.createReadStream('/home/srikanthkr/typescript_project/source_data/company_master_data_upto_Mar_2015_Maharashtra.csv')
    .pipe(csv())
    .on('data', function (data) {
    var y;
    var date = data.DATE_OF_REGISTRATION.split('-');
    if (date[2] >= '2010' && date[2] <= '2018') {
        if (!PBARegistrationsPerYear[data.PRINCIPAL_BUSINESS_ACTIVITY]) {
            PBARegistrationsPerYear[data.PRINCIPAL_BUSINESS_ACTIVITY] = {};
            y = PBARegistrationsPerYear[data.PRINCIPAL_BUSINESS_ACTIVITY];
            if (!y[date[2]]) {
                y[date[2]] = 1;
            }
        }
        else {
            y = PBARegistrationsPerYear[data.PRINCIPAL_BUSINESS_ACTIVITY];
            if (!y[date[2]]) {
                y[date[2]] = 1;
            }
            else {
                y[date[2]] += 1;
            }
        }
    }
})
    .on('end', function () {
    fs.writeFile('/home/srikanthkr/typescript_project/frontend/json/Year_Activity.json', JSON.stringify(PBARegistrationsPerYear), function () { });
});
