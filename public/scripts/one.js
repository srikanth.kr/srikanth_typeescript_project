"use strict";
var fs = require('fs');
var csv = require('csv-parser');
var AUTHORIZED_CAPResult = [0, 0, 0, 0, 0];
fs.createReadStream('/home/srikanthkr/typescript_project/source_data/company_master_data_upto_Mar_2015_Maharashtra.csv')
    .pipe(csv())
    .on('data', function (res) {
    if (res.AUTHORIZED_CAP < 1e5)
        AUTHORIZED_CAPResult[0]++;
    if (res.AUTHORIZED_CAP >= 1e5 && res.AUTHORIZED_CAP < 1e6)
        AUTHORIZED_CAPResult[1]++;
    if (res.AUTHORIZED_CAP >= 1e6 && res.AUTHORIZED_CAP < 1e7)
        AUTHORIZED_CAPResult[2]++;
    if (res.AUTHORIZED_CAP >= 1e8 && res.AUTHORIZED_CAP < 1e9)
        AUTHORIZED_CAPResult[3]++;
    if (res.AUTHORIZED_CAP > 1e9)
        AUTHORIZED_CAPResult[4]++;
})
    .on('end', function () {
    console.log(AUTHORIZED_CAPResult);
    fs.writeFile('/home/srikanthkr/typescript_project/frontend/json/AUTHORIZED_CAP_graph.json', JSON.stringify(AUTHORIZED_CAPResult), function () { });
});
