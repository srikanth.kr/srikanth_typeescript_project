fetch('frontend/build/json/Year_Activity.json')
.then(function(response) {
    response.json().then(function(result) {
    const year = [2010];
    for (let i = 0 ; year[i] <= 2014 ; i++){
      year.push(1+year[i]);
    }
    console.log(year)
    const objects = [];
    const key1 = (<any>Object).keys(result);
    console.log(key1)
    for (let i = 0 ; i < key1.length ; i++){
      objects.push({
        name:key1[i],
        data:(<any>Object).values(result[key1[i]]),
      },);
    }
    console.log(objects)
    Highcharts.chart('chart4', {
      chart:{
        type:'bar'
      },
      title:{
        text:'STACKED_BAR_CHART'
      },
      xAxis:{
        categories:year,
        title:{
          text:'YEAR',
        }
      },
      yAxis:{
        title:{
          text:'NO_PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_YEAR'
        }
      },
      plotOptions:{
        series:{
          stacking:'normal',
        },
      },
      series:objects,
    });
  });
});