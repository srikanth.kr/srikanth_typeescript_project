"use strict";
fetch('frontend/build/json/PBARegistrations_2015.json')
    .then(function (response) {
    response.json().then(function (data) {
        var keysSorted = Object.keys(data).sort(function (a, b) { return data[b] - data[a]; });
        var pba = [];
        var numofreg = [];
        for (var i = 0; i < 10; i += 1) {
            pba.push(keysSorted[i]);
            numofreg.push(data[keysSorted[i]]);
        }
        Highcharts.chart('chart3', {
            chart: {
                type: 'column',
            },
            title: {
                text: 'Top registrations by "Principal Business Activity" for the year 2015',
            },
            xAxis: {
                title: {
                    text: 'Principal Business Activity ---->',
                },
                categories: pba,
            },
            yAxis: {
                title: {
                    text: 'Number of Company Registrations ---->',
                },
            },
            plotOptions: {
                column: {
                    dataLabels: {
                        enabled: true,
                    },
                },
            },
            series: [{
                    name: 'Number of Company Registrations',
                    data: numofreg,
                }],
        });
    });
});
