fetch('frontend/build/json/PBARegistrations_2015.json')
    .then(function(response) {
        response.json().then(function(data) {
            const keysSorted = Object.keys(data).sort((a, b) => data[b] - data[a]);
            const pba = [];
            const numofreg = [];
            for (let i = 0; i < 10; i += 1) {
              pba.push(keysSorted[i]);
              numofreg.push(data[keysSorted[i]]);
            }
            Highcharts.chart('chart3', {
              chart: {
                type: 'column',
              },
              title: {
                text: 'Top registrations by "Principal Business Activity" for the year 2015',
              },
              xAxis: {
                title: {
                  text: 'Principal Business Activity ---->',
                },
                categories: pba,
              },
              yAxis: {
                title: {
                  text: 'Number of Company Registrations ---->',
                },
              },
              plotOptions: {
                column: {
                  dataLabels: {
                    enabled: true,
                  },
                },
              },
              series: [{
                name: 'Number of Company Registrations',
                data: numofreg,
              }],
  })
})
    })