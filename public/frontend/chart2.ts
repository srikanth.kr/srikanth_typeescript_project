fetch('frontend/build/json/Number_OfRegistrationPerYear.json')
    .then(function(response) {
        response.json().then(function(data) {
            const year = Object.keys(data); 
            const reg = (<any>Object).values(data);
          Highcharts.chart('chart2', {
            chart: {
              type: 'column',
            },
            title: {
              text: 'Company Registration per year',
            },
            xAxis: {
              title: {
                text: 'Year -->',
              },
              categories: year,
            },
            yAxis: {
              title: {
                text: 'Number of Company Registrations -->',
              },
            },
            plotOptions: {
              column: {
                dataLabels: {
                  enabled: true,
                },
              },
            },
            series: [{
              name: 'Number of Company Registrations',
              data: reg,
            }],     
        })
})
})